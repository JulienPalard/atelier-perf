SRCS := $(wildcard *.md)
HTML := $(SRCS:.md=.html)

.PHONY: static
static: $(HTML)

%.html: %.md $(REVEAL_JS)
	mdtoreveal $< -o $@
	sed -i 's/<style>/\0.reveal .hljs-ln-numbers{display: none};/' $@

.PHONY: clean
clean:
	rm -f *.html

.PHONY: hyperfine
hyperfine:
	for i in $$(seq 2 7) ; do hyperfine --show-output "python perf.py --version $i AFPy 00000" > hyperfine-AFPy.$i; done

.PHONY: profiles
profiles:
	python -m cProfile -o prof perf.py AFPy 0000 --version 1
	for i in $$(seq 2 7) ; do python -m cProfile -o $$i.prof perf.py --version $$i AFPy 00000; done
