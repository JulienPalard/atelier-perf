import sys
from itertools import product
from string import ascii_lowercase, ascii_uppercase
import hashlib


def main():
    string = "AFPy".encode("UTF-8")
    pool = (ascii_uppercase + "[\\]^_`" + ascii_lowercase).encode("UTF-8")
    for c in product(pool, repeat=10):
        candidate = bytes(c) + string
        digest = hashlib.sha512(candidate).hexdigest()
        if digest.startswith("00000"):
            print(f"sha512({candidate}) = {digest}")
            sys.exit(0)


if __name__ == "__main__":
    main()
