#!/bin/sh
echo Pure C

FLAG="--style basic"

# Use --show-output to validate the results
# FLAG="--show-output"


cc -O3 myperf.c -o myperf -lcrypto
hyperfine $FLAG ./myperf

echo Cython sans hashlib

python setup.py build_ext --inplace >/dev/null
hyperfine $FLAG "python3 -c 'from perf import search; search()'"

echo Cython avec hashlib

hyperfine $FLAG "python3 -c 'from perf_hashlib import search; search()'"

echo Pure Python

hyperfine $FLAG 'python3 perf.py'

echo Pypy 7.3.3

hyperfine $FLAG 'pypy3 perf.py'
